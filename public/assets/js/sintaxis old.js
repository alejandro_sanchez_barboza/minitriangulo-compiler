$(document).ready(function(){

    var operatorsPositions = [];
    var fields = [];

    function inOperator(incomming, operator){
        if(operator != "submit"){
            if(operator == " " || operator == "lineBreak"){ 
                incomming = incomming.substring(0,incomming.length-1); 
                fields.push(incomming.substring(0,incomming.length - 1));
            } 
            else{ 
                fields.push(incomming.substring(0,incomming.length - 1));
                fields.push(operator); 
            }
            operatorsPositions.length == 0 ? operatorsPositions.push(incomming.length) : operatorsPositions.push(incomming.length + operatorsPositions[operatorsPositions.length-1]);
        }
        else{
            fields.push(incomming.substring(0,incomming.length));
        }

        console.log("Incomming: '" + incomming + "'");
        console.log("Operator: '" + operator + "'");

        values = "";
        $.each(fields, function(k, v) {
            values = values + "'" + v + "' , ";
        });
        //console.log("Fields: " + values);

        if(
            incomming == "begin" || 
            incomming == "in" || 
            incomming == "const" || 
            incomming == "let" || 
            incomming == "do" || 
            incomming == "then" || 
            incomming == "else" || 
            incomming == "var" || 
            incomming == "end" || 
            incomming == "while" || 
            incomming == "if"
        ){
            console.log("Terminal");
            console.log("Position: " + operatorsPositions[operatorsPositions.length-1]);
        }
        /*
        $.each(operatorsPositions, function(k, v) {
            values = values + "'" + v + "' , ";
        });
        */
    }
    
    $('.compiler .codigo').bind('input propertychange', function() {
        while($(this).val().length < operatorsPositions[operatorsPositions.length-1]){
            operatorsPositions.pop();
            fields.pop();
        }
        operatorsPositions.length == 0 ? incomming = $(this).val() : incomming = $(this).val().substring(operatorsPositions[operatorsPositions.length-1]);

        if(incomming.indexOf(" ") > 0){ inOperator(incomming, " "); }
        else if(incomming.indexOf(";") > 0){ inOperator(incomming, ";"); }
        else if(incomming.indexOf(":") > 0){ inOperator(incomming, ":"); }
        else if(incomming.indexOf("~") > 0){ inOperator(incomming, "~"); }
        else if(incomming.indexOf("(") > 0){ inOperator(incomming, "("); }
        else if(incomming.indexOf(")") > 0){ inOperator(incomming, ")"); }
        else if(incomming.indexOf("+") > 0){ inOperator(incomming, "+"); }
        else if(incomming.indexOf("-") > 0){ inOperator(incomming, "-"); }
        else if(incomming.indexOf("*") > 0){ inOperator(incomming, "*"); }
        else if(incomming.indexOf("/") > 0){ inOperator(incomming, "/"); }
        else if(incomming.indexOf("<") > 0){ inOperator(incomming, "<"); }
        else if(incomming.indexOf(">") > 0){ inOperator(incomming, ">"); }
        else if(incomming.indexOf("=") > 0){ inOperator(incomming, "="); }
        else if(incomming.indexOf("\\") > 0){ inOperator(incomming, "\\"); }
        else if(incomming.indexOf("\n") > 0){ inOperator(incomming, "lineBreak"); }

    });


    $(".compiler form button[type=submit]").click(function(event){
        event.preventDefault();
        while($('.compiler .codigo').val().length < operatorsPositions[operatorsPositions.length-1]){operatorsPositions.pop();fields.pop();}
        operatorsPositions.length == 0 ? incomming = $('.compiler .codigo').val() : incomming = $('.compiler .codigo').val().substring(operatorsPositions[operatorsPositions.length-1]);
        inOperator(incomming, "submit");

        values = "";
        $.each(fields, function(k, v) {
            values = values + "'" + v + "' , ";
        });
        $(".sintaxis-array").html(values);
    });
});

function type(value){
    var digit = new RegExp('^[0-9]$');
    var letter = new RegExp('^[A-Za-z]$');
    var operator = new RegExp(/;|:|~|(|)|\+|-|\*|\/|<|>|=\\/);
    var terminal = new RegExp(/(begin|in|const|let|do|then|else|var|end|while|if)/);
    if (digit.test(value)) {
        return 0;
    }
    else if(letter.test(value)){
        return 1;
    }
    else if(operator.test(value)){
        console.log(operator.test(value));
        return 2;
    }
    else if(terminal.test(value)){
        return 3;
    }
    else{
        return 4;
    }
}
$(document).ready(function(){
    $(".compiler form button[type=submit]").click(function(event){
        event.preventDefault();
        $(".analisislexico").html("Analisis Lexico: " + analisisLexico().join(" "))
        analisisSintactico(analisisLexico())
    });
});

function analisisLexico(){
    var fields = [];
    var code = $(".compiler .code").val() + " ";
    var char = '';
    for(var i = 0; i < code.length; i++){
        char = code.charAt(i);
        switch(type(char)){
            case 0:
                if(fields.length > 0 && type(code.charAt(i-1)) == 0){
                    fields[fields.length-1] = "" + fields[fields.length-1] + char;
                }
                else{
                    fields.push("INTEGER-LITERAL");
                    fields.push(char);
                }
                break;
            case 1:
                if(fields.length > 0 && type(code.charAt(i-1)) == 1){
                    fields[fields.length-1] = fields[fields.length-1] + char;
                }
                else{
                    fields.push("IDENTIFIER");
                    fields.push(char);
                }
                break;
            case 2:
                if(fields.length > 0 && char == "=" && code.charAt(i-1) == ":"){
                    fields.pop();
                    fields[fields.length-1] = ":=";
                }
                else if(char == ";" || char == "(" || char == ")"){
                    fields.push(char);    
                }
                else{
                    fields.push("OPERATOR");
                    fields.push(char);
                }
                break;
            case 3:
                if(type(fields[fields.length-1]) == 4 && fields[fields.length-2] == "IDENTIFIER"){
                    var reserved = fields.pop();
                    fields[fields.length-1] = reserved;
                }
                break;
        }
    }
    return fields;
}

function type(value){
    if ($.isNumeric(value)) {
        return 0; // Digito
    }
    else if(
        value == ';' ||
        value == ':' ||
        value == '~' ||
        value == '(' ||
        value == ')' ||
        value == '+' ||
        value == '-' ||
        value == '*' ||
        value == '/' ||
        value == '\\' ||
        value == '<' ||
        value == '>' ||
        value == '='
    ){
        return 2; // Operador
    }
    else if(value == " "){
        return 3; // Espacio Vacio
    }
    else if(
        value == 'begin' ||
        value == 'in' ||
        value == 'const' ||
        value == 'let' ||
        value == 'do' ||
        value == 'then' ||
        value == 'else' ||
        value == 'var' ||
        value == 'end' ||
        value == 'while' ||
        value == 'if'
    ){
        return 4; // Palabra reservada
    }
    else{
        return 1; // Letra
    }
}

function treeNode(name = "", children = ""){
    `{ \
        text: { name: "${name}"}, \
        children: [ ${treeNode()}] \
    }` 
}

function analisisSintactico(input){

    var papas = [];
    var ramas = [];
    var tempArray = [];       
 
    for(i=0; i<input.length; i++){

        if(input[i] == "IDENTIFIER" || input[i] == "OPERATOR" || input[i] == "INTEGER-LITERAL"){
            tempArray.push(input[i+1]);
            fillTempArray(input[i]);
            papas.push(tempArray[tempArray.length-1]);
            ramas.push(getRama(tempArray));
        }
        else if(
            input[i] == ":=" || 
            input[i] == ";" || 
            input[i] == "(" || 
            input[i] == ")" || 
            input[i] == "begin" || 
            input[i] == "in" || 
            input[i] == "const" || 
            input[i] == "let" || 
            input[i] == "do" || 
            input[i] == "then" || 
            input[i] == "else" || 
            input[i] == "var" || 
            input[i] == "end" || 
            input[i] == "while" || 
            input[i] == "if"){
            tempArray.push(input[i]);
            papas.push(input[i]);
            ramas.push(getRama(tempArray));
        }
    }

    //AQUI VOY A UNIR LAS RAMAS

    var whileCont = 0;

    console.log(papas.join(" "));
    
    //Caso :=
    while($.inArray(":=", papas) > -1 && whileCont < 5){
        lookFirst();
        whileCont++;
    }
    whileCont = 0;
    function lookFirst(){
        var index = $.inArray(":=", papas);
        if(index > -1){
            if(ramas[index-1].indexOf("V-NAME") > -1){
                ramas[index-1] = ramas[index-1].replace('{ text: { name: "EXPRESSION"}, children: [ { text: { name: "PRIMARY-EXPRESSION"}, children: [ ','');
                ramas[index-1] = ramas[index-1].replace('] }] }','');
                papas[index-1] = "V-NAME";
                mievaluacion = papas[index-1] + " " + papas[index] + " " + papas[index+1];
                mregla = regla(mievaluacion);
                if(mregla != ""){
                    var ramatemporal = `{ text: { name: "${mregla}"}, children: [ `;
                    for(j = index-1; j<=index+1; j++){ ramatemporal = ramatemporal + ramas[j] + ","; }
                    ramatemporal = ramatemporal + `] }`;
                    papas[index-1] = mregla;
                    papas.splice(index,1);
                    papas.splice(index,1);
                    ramas[index-1] = ramatemporal;
                    ramas.splice(index,1);
                    ramas.splice(index,1);
                }
                mievaluacion = mregla;
                if(mievaluacion == "SINGLE-COMMAND"){
                    mregla = regla(mievaluacion);
                    if(mregla != ""){
                        var ramatemporal = `{ text: { name: "${mregla}"}, children: [ `;
                        ramatemporal = ramatemporal + ramas[index-1] + ",";
                        ramatemporal = ramatemporal + `] }`;
                        papas[index-1] = mregla;
                        ramas[index-1] = ramatemporal;
                    }
                }
            }
        }
    }

    //Caso ;
    while($.inArray(";", papas) > -1 && whileCont < 5){
        lookSecond();
        whileCont++;
    }
    whileCont = 0;
    function lookSecond(){
        var index = $.inArray(";", papas);
        if(index > -1){
            if(ramas[index+1].indexOf("COMMAND") > -1){
                ramas[index+1] = ramas[index+1].replace(`{ text: { name: "COMMAND"}, children: [ `,'');
                ramas[index+1] = ramas[index+1].slice(0,-4);
                papas[index+1] = "SINGLE-COMMAND";
                mievaluacion = papas[index-1] + " " + papas[index] + " " + papas[index+1];
                mregla = regla(mievaluacion);
                if(mregla != ""){
                    var ramatemporal = `{ text: { name: "${mregla}"}, children: [ `;
                    for(j = index-1; j<=index+1; j++){ 
                        ramatemporal = ramatemporal + ramas[j] + ","; 
                    }

                    ramatemporal = ramatemporal + `] }`;
                    papas[index-1] = mregla;
                    papas.splice(index,1);
                    papas.splice(index,1);
                    ramas[index-1] = ramatemporal;
                    ramas.splice(index,1);
                    ramas.splice(index,1);
                }
            }
        }
    }

    //Caso Begin
    while($.inArray("begin", papas) > -1 && whileCont < 5){
        lookThird();
        whileCont++;
    }
    whileCont = 0;
    function lookThird(){
        var index = $.inArray("begin", papas);
        if(index > -1){
            mievaluacion = papas[index] + " " + papas[index+1] + " " + papas[index+2];
            mregla = regla(mievaluacion);
            if(mregla != ""){
                var ramatemporal = `{ text: { name: "${mregla}"}, children: [ `;

                for(j = index; j<=index+2; j++){ 
                        ramatemporal = ramatemporal + ramas[j] + ","; 
                    }
                    ramatemporal = ramatemporal + `] }`;
                    papas[index] = mregla;
                    papas.splice(index+1,1);
                    papas.splice(index+1,1);
                    ramas[index] = ramatemporal;
                    ramas.splice(index+1,1);
                    ramas.splice(index+1,1);
                }
        }
    }

    //Caso ()
    while($.inArray("(", papas) > -1 && whileCont < 5){
        lookFourth();
        whileCont++;
    }
    whileCont = 0;
    function lookFourth(){
        var index = $.inArray("(", papas);
        if(index > -1){
            var findex = $.inArray(")", papas);
            if(findex > -1){
                var mievaluacion = "";
                for (r=index+1; r-findex;r++){
                    mievaluacion = mievaluacion + " " + papas[r];
                }
            }
            mievaluacion =  $.trim(mievaluacion);

            if(mievaluacion == "EXPRESSION OPERATOR EXPRESSION"){
                papas[index+3] = "PRIMARY-EXPRESSION";
                ramas[index+3] = ramas[index+3].replace('text: { name: "EXPRESSION"}, children: [ { ','');
                ramas[index+3] = ramas[index+3].replace('}]','');
            }

            mregla = regla(mievaluacion);
            
            if(mregla != ""){
                var ramatemporal = `{ text: { name: "${mregla}"}, children: [ `;
                for(j = index; j<=findex; j++){ ramatemporal = ramatemporal + ramas[j] + ","; }
                ramatemporal = ramatemporal + `] } ] } ] }`;
                console.log(ramatemporal);
                console.log("Antes: " + papas.join());
                papas[index] = mregla;
                ramas[index] = ramatemporal;
                papas.splice(index+1,1);
                ramas.splice(index+1,1);
                papas.splice(index+1,1);
                ramas.splice(index+1,1);
                papas.splice(index+1,1);
                ramas.splice(index+1,1);
                papas.splice(index+1,1);
                ramas.splice(index+1,1);
                console.log("Despues: " + papas.join());
            }
        }
    }

    console.log("Array Indices: " + papas.join( " "));
    console.log("Ramas:");
    for(k=0; k<ramas.length;k++){
        console.log(ramas[k]);
    }
    console.log("-----------------------------------------------");

    //Merge general
    mievaluacion = papas[0];
    for(i = 1; i<papas.length; i++){
        // Degradando Regla Var
        if(papas[i] == "EXPRESSION" && papas[i-1] == "var"){
            papas[i] = "IDENTIFIER";
            ramas[i] = ramas[i].replace('text: { name: "EXPRESSION"}, children: [ { ','');
            ramas[i] = ramas[i].replace('}]','');
            ramas[i] = ramas[i].replace('text: { name: "PRIMARY-EXPRESSION"}, children: [ { ','');
            ramas[i] = ramas[i].replace('}]','');
            ramas[i] = ramas[i].replace('text: { name: "V-NAME"}, children: [ { ','');
            ramas[i] = ramas[i].replace('}]','');
        }
        // Degradando Regla Expression Operator Expression
        if(papas[i] == "EXPRESSION" && mievaluacion == "EXPRESSION OPERATOR"){
            papas[i] = "PRIMARY-EXPRESSION";
            ramas[i] = ramas[i].replace('text: { name: "EXPRESSION"}, children: [ { ','');
            ramas[i] = ramas[i].replace('}]','');
        }
        // Degradando Regla if - then
        if(papas[i] == "COMMAND" && mievaluacion == "if EXPRESSION then"){
            papas[i] = "SINGLE-COMMAND";
            ramas[i] = ramas[i].replace('text: { name: "COMMAND"}, children: [ { ','');
            ramas[i] = ramas[i].replace('}]','');
        }
        // Degradando Regla then-else
        if(papas[i] == "COMMAND" && mievaluacion == "if EXPRESSION then SINGLE-COMMAND else"){
            papas[i] = "SINGLE-COMMAND";
            ramas[i] = ramas[i].replace('text: { name: "COMMAND"}, children: [ { ','');
            ramas[i] = ramas[i].replace('}]','');
        }

        // Uniendo Ramas
        papas.length > 1 ? mievaluacion = mievaluacion + " " + papas[i] : null ;

        if($.inArray("var", papas)){
            var thisindex = $.inArray("var", papas);
            var tempevaluacion = papas[thisindex] + " " + papas[thisindex+1];
            mregla = regla(tempevaluacion);
            if(mregla != ""){
                mievaluacion = mievaluacion.replace(tempevaluacion,"");
                var ramatemporal = `{ text: { name: "${mregla}"}, children: [ `;
                for(j = 0; j<=i; j++){ ramatemporal = ramatemporal + ramas[j] + ","; }
                ramatemporal = ramatemporal + `] }`;
                papas[thisindex] = mregla;
                ramas[thisindex] = ramatemporal;
                papas.splice(thisindex+1,1);
                ramas.splice(thisindex+1,1);
                mievaluacion = mievaluacion + mregla;
            }
        }

        // Generales
        mregla = regla(mievaluacion);
        if(mregla != ""){
            var ramatemporal = `{ text: { name: "${mregla}"}, children: [ `;
            for(j = 0; j<=i; j++){ ramatemporal = ramatemporal + ramas[j] + ","; }
            ramatemporal = ramatemporal + `] }`;
            papas[0] = mregla;
            ramas[0] = ramatemporal;
            while(i > 0){
                papas.splice(i,1);
                ramas.splice(i,1);
                i--;
            }
            mievaluacion = mregla;
        }
    }
    // Termina la que funciona para numeros

    //Generacion del arbol
    var jsonConfig = '({chart:{container: "#basic-example", connectors: { type: "step" }, node: { HTMLclass: "nodeExample1"}},nodeStructure:'; 
    jsonConfig = jsonConfig + ramas[0];
    jsonConfig = jsonConfig + '})';

    console.log("json: " + jsonConfig);

    if(ramas.length > 1){
        $(".errorsintaxis").html("Error de sintaxis con la expression:<br>" + mievaluacion);
        $(".errorsintaxis").show();
        $(".chart").hide();
    }
    else{
        $(".errorsintaxis").hide();
        $(".chart").show();
        var chart_config = eval(jsonConfig);
        new Treant( chart_config );
    }

    function getRama(array){
        if(array.length != 0){ return `{ text: { name: "${array.pop()}"}, children: [ ${getRama(array)}] }`; }
        else{ return ''; }
    }

    function fillTempArray(name){
        if(name != ""){
            tempArray.push(name);
            fillTempArray(regla(name));
        }
    }
    
}

function regla(input){
    switch(input){
        case "COMMAND ; SINGLE-COMMAND": 
            return "COMMAND";
            break;
        case "SINGLE-COMMAND": 
            return "COMMAND";
            break;
        case "V-NAME := EXPRESSION": 
            return "SINGLE-COMMAND";
            break;
        case "IDENTIFIER ( EXPRESSION )": 
            return "SINGLE-COMMAND";
            break;
        case "if EXPRESSION then SINGLE-COMMAND else SINGLE-COMMAND": 
            return "SINGLE-COMMAND";
            break;
        case "while EXPRESSION do SINGLE-COMMAND": 
            return "SINGLE-COMMAND";
            break;
        case "let DECLARATION in SINGLE-COMMAND": 
            return "SINGLE-COMMAND";
            break;
        case "begin COMMAND end": 
            return "SINGLE-COMMAND";
            break;
        case "PRIMARY-EXPRESSION": 
            return "EXPRESSION";
            break;
        case "EXPRESSION OPERATOR PRIMARY-EXPRESSION": 
            return "EXPRESSION";
            break;
        case "INTEGER-LITERAL": 
            return "PRIMARY-EXPRESSION";
            break;
        case "V-NAME": 
            return "PRIMARY-EXPRESSION";
            break;
        case "OPERATOR PRIMARY-EXPRESSION": 
            return "PRIMARY-EXPRESSION";
            break;
        case "( EXPRESSION )": 
            return "PRIMARY-EXPRESSION";
            break;
        case "IDENTIFIER": 
            return "V-NAME";
            break;
        case "SINGLE-DECLARATION": 
            return "DECLARATION";
            break;
        case "DECLARATION ; SINGLE-DECLARATION": 
            return "DECLARATION";
            break;
        case "const IDENTIFIER ~ EXPRESSION": 
            return "SINGLE-DECLARATION";
            break;
        case "var IDENTIFIER : TYPE-DENOTER": 
            return "SINGLE-DECLARATION";
            break;
        case "var IDENTIFIER": 
            return "SINGLE-DECLARATION";
            break;
        case "IDENTIFIER": 
            return "TYPE-DENOTER";
            break;
        case "+": 
            return "OPERATOR";
            break;
        case "-": 
            return "OPERATOR";
            break;
        case "*": 
            return "OPERATOR";
            break;
        case "/": 
            return "OPERATOR";
            break;
        case "<": 
            return "OPERATOR";
            break;
        case ">": 
            return "OPERATOR";
            break;
        case "=": 
            return "OPERATOR";
            break;
        case "LETTER": 
            return "IDENTIFIER";
            break;
        case "IDENTIFIER LETTER": 
            return "IDENTIFIER";
            break;
        case "IDENTIFIER DIGIT": 
            return "IDENTIFIER";
            break;
        case "DIGIT": 
            return "INTEGER-LITERAL";
            break;
        case "INTEGER-LITERAL DIGIT": 
            return "INTEGER-LITERAL";
            break;
        case "! Graphic* eol": 
            return "COMMENT";
            break;
        default:
            return "";
            break;
    }
}
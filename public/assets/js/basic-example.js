
var config = { container: "#basic-example", connectors: { type: 'step' }, node: { HTMLclass: 'nodeExample1' } },
    
    expression1             = {text:{name:"EXPRESSION"}},
    expression2             = {text:{name:"EXPRESSION"},            parent:expression1},
    expression3             = {text:{name:"EXPRESSION"},            parent:expression2},
    primaryexpression1      = {text:{name:"PRIMARY-EXPRESSION"},    parent:expression3},
    vname1                  = {text:{name:"V-NAME"},                parent:primaryexpression1},
    identifier1             = {text:{name:"IDENTIFIER"},            parent:vname1},
    ident1                  = {text:{name:"d"},                     parent:identifier1},
    operator1               = {text:{name:"OPERATOR"},              parent:expression2},
    op1                     = {text:{name:"+"},                     parent:operator1},
    primaryexpression2      = {text:{name:"PRIMARY-EXPRESSION"},    parent:expression2},
    integerliteral1         = {text:{name:"INTEGER-LITERAL"},       parent:primaryexpression2},
    int1                    = {text:{name:"10"},                    parent:integerliteral1},
    operator2               = {text:{name:"OPERATOR"},              parent:expression1}, 
    op2                     = {text:{name:"*"},                     parent:operator2},
    primaryexpression3      = {text:{name:"PRIMARY-EXPRESSION"},    parent:expression1},
    vname2                  = {text:{name:"V-NAME"},                parent:primaryexpression3},
    identifier2              = {text:{name:"IDENTIFIER"},            parent:vname2},
    ident2                  = {text:{name:"n"},                     parent:identifier2},
    
    chart_config = [ config,
        expression1,
        expression2,
        expression3,
        primaryexpression1,
        vname1,
        identifier1,
        ident1,
        operator1,
        op1,
        primaryexpression2,
        integerliteral1,
        int1,
        operator2,
        op2,
        primaryexpression3,
        vname2,
        identifier2,
        ident2,
    ];